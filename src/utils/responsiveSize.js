import {Dimensions, Platform, PixelRatio} from 'react-native';
const width = Dimensions.get('window').width;

const scale = width > 450 ? width / 400 : width / 320;

function normalize(size) {
  const newSize = size * scale;
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
  }
}

export default {
  FONT_SIZE_XXSMALL: normalize(10),
  FONT_SIZE_XSMALL: normalize(12),
  FONT_SIZE_SMALL: normalize(14),
  FONT_SIZE_MEDIUM: normalize(15),
  FONT_SIZE_LARGE: normalize(17),
  FONT_SIZE_XLARGE: normalize(19),
  IMAGE_SIZE_SMALL: normalize(40),
};
