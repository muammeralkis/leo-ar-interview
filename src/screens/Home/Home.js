import React from 'react';
import {StatusBar, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

import Header from '../../components/_common/Header/Header';
import Categories from '../../components/home/Categories/Categories';
import List from '../../components/home/List/List';

import styles from './homeStyles';

const Home = () => {
  return (
    <SafeAreaView style={styles.safeArea}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <View style={styles.container}>
        <Header />
        <Categories />
        <List />
      </View>
    </SafeAreaView>
  );
};

export default Home;
