import {StyleSheet} from 'react-native';
import responsiveSize from '../../utils/responsiveSize';

export default StyleSheet.create({
  safeArea: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
    fontSize: responsiveSize.FONT_SIZE_XSMALL,
  },
});
