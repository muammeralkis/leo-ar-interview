import React from 'react';
import {Text} from 'react-native';

import styles from './labelStyles';

const Label = ({label}) => {
  return <Text style={styles.label}>{label}</Text>;
};

export default Label;
