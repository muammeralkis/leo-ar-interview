import {StyleSheet} from 'react-native';
import responsiveSize from '../../../utils/responsiveSize';

export default StyleSheet.create({
  label: {
    fontWeight: '400',
    color:"#3d3d3d",
    fontSize: responsiveSize.FONT_SIZE_XSMALL,
  },
});
