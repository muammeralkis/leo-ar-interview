import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

import Label from '../Label/Label';

import styles from './headerStyles';

const Header = () => {
  return (
    <View style={styles.container}>
      <TouchableOpacity>
        <Label label="Cancel" />
      </TouchableOpacity>
      <Text style={styles.title}>Choose Music</Text>
      <TouchableOpacity>
        <Label label="Done" />
      </TouchableOpacity>
    </View>
  );
};

export default Header;
