import {StyleSheet} from 'react-native';
import responsiveSize from '../../../utils/responsiveSize';

export default StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: 50,
    paddingHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  title: {
    fontSize: responsiveSize.FONT_SIZE_MEDIUM,
    color: '#494949',
    fontWeight: '600',
  },
});
