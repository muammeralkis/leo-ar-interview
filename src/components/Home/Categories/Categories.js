import React from 'react';
import {FlatList, View} from 'react-native';

import allCategories from '../../../constants/allCategories.json';
import Category from './Category/Category';

import styles from './categoriesStyles';

const Categories = () => {
  const renderItem = ({item, index}) => <Category item={item} index={index} />;

  return (
    <View style={styles.container}>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={allCategories}
        renderItem={renderItem}
      />
    </View>
  );
};

export default Categories;
