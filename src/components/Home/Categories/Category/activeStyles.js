import {StyleSheet} from 'react-native';
import responsiveSize from '../../../../utils/responsiveSize';

export default StyleSheet.create({
  container: {
    borderBottomWidth: 1.5,
    borderBottomColor: '#363636',
    paddingHorizontal: 10,
    justifyContent: 'center',
  },
  title: {
    color: '#363636',
    fontSize: responsiveSize.FONT_SIZE_MEDIUM,
    fontWeight: '500',
  },
});
