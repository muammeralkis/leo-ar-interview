import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

import activeStyles from '../Category/activeStyles';
import inactiveStyles from '../Category/inactiveStyles';

const Category = ({item, index}) => {
  const styles = index === 0 ? activeStyles : inactiveStyles;
  return (
    <TouchableOpacity style={styles.container}>
      <Text style={styles.title}>{item}</Text>
    </TouchableOpacity>
  );
};

export default Category;
