import {StyleSheet} from 'react-native';
import responsiveSize from '../../../../utils/responsiveSize';

export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginHorizontal: 15,
  },
  title: {
    fontSize: responsiveSize.FONT_SIZE_MEDIUM,
    color:"#b0b0b0",
    fontWeight: '500',
  },
});
