import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    position: 'relative',
    top: 50,
    height: 60,
    backgroundColor: 'white',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.18,
    shadowRadius: 8,
    elevation: 10,
  },
});
