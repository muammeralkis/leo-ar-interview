import React from 'react';
import {View, TouchableOpacity} from 'react-native';

import Thumbnail from './Thumbnail/Thumbnail';
import Info from './Info/Info';
import Button from './Button/Button';

import styles from './listItemStyles';

const ListItem = ({item}) => {
  return (
    <TouchableOpacity style={styles.container}>
      <Thumbnail thumbUrl={item.thumbUrl} />
      <View style={styles.contentContainer}>
        <Info name={item.name} artistName={item.artistName} tags={item.tags} />
        <Button />
      </View>
    </TouchableOpacity>
  );
};

export default ListItem;
