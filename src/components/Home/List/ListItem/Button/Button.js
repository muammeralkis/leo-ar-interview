import React from 'react';
import {TouchableOpacity, Text} from 'react-native';

import styles from './buttonStyles';

const Button = () => {
  return (
    <TouchableOpacity style={styles.buttonContainer}>
      <Text style={styles.label}>Choose</Text>
    </TouchableOpacity>
  );
};

export default Button;
