import {StyleSheet} from 'react-native';
import responsiveSize from '../../../../../utils/responsiveSize';

export default StyleSheet.create({
  buttonContainer: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    borderRadius: 30,
    paddingVertical: 12.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    fontWeight: '500',
    color: '#494949',
    fontSize: responsiveSize.FONT_SIZE_XSMALL,
  },
});
