import React from 'react';
import {View, Text} from 'react-native';

import styles from './infoStyles';

const Info = ({name, artistName, tags}) => {
  const capitalize = str => str.charAt(0).toUpperCase() + str.slice(1);

  return (
    <View style={styles.infoContainer}>
      <Text style={styles.name}>{name}</Text>
      <Text style={styles.detail}>{artistName}</Text>
      <Text style={styles.detail}>
        {tags.map(tag => '#' + capitalize(tag) + ' ')}
      </Text>
    </View>
  );
};

export default Info;
