import {StyleSheet} from 'react-native';
import responsiveSize from '../../../../../utils/responsiveSize';

export default StyleSheet.create({
  infoContainer: {
    flex: 2.5,
    paddingRight: 20,
    justifyContent: 'center',
    marginLeft: 10,
  },
  name: {
    color: '#494949',
    fontSize: responsiveSize.FONT_SIZE_XSMALL,
    fontWeight: '700',
  },
  detail: {
    fontSize: responsiveSize.FONT_SIZE_XXSMALL,
    color: '#858585',
    fontWeight: '500',
  },
});
