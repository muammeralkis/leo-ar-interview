import {StyleSheet} from 'react-native';
import responsiveSize from '../../../../../utils/responsiveSize';

export default StyleSheet.create({
  thumbContainer: {
    borderWidth: 2,
    borderColor: '#d1d1d1',
    maxHeight: responsiveSize.IMAGE_SIZE_SMALL + 20,
    borderRadius: 13,
    justifyContent: 'center',
    padding: 3,
  },
  thumb: {
    width: responsiveSize.IMAGE_SIZE_SMALL,
    height: responsiveSize.IMAGE_SIZE_SMALL,
    alignItems: 'center',
    justifyContent: 'center',
  },
  thumbImage: {
    opacity: 0.8,
  },
  playIcon: {
    width: 25,
    height: 25,
  },
});
