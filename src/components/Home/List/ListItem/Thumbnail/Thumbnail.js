import React from 'react';
import {View, Image, ImageBackground} from 'react-native';

import styles from './thumbnailStyles';

const Thumbnail = ({thumbUrl}) => {
  return (
    <View style={styles.thumbContainer}>
      <ImageBackground
        source={{uri: thumbUrl}}
        borderRadius={13}
        imageStyle={styles.thumbImage}
        style={styles.thumb}>
        <Image
          style={styles.playIcon}
          source={require('../../../../../assets/playIcon.png')}
        />
      </ImageBackground>
    </View>
  );
};

export default Thumbnail;
