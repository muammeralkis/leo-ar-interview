import React from 'react';
import {FlatList} from 'react-native';

import dummyData from '../../../constants/dummyData.json';
import ListItem from './ListItem/ListItem';
import styles from './listStyles';

const List = () => {
  const renderItem = ({item}) => <ListItem item={item} />;
  return (
    <FlatList
      style={styles.container}
      contentContainerStyle={styles.contentContainer}
      data={Object.values(dummyData.data)}
      renderItem={renderItem}
    />
  );
};

export default List;
