import {StyleSheet, Platform} from 'react-native';
import responsiveSize from '../../../utils/responsiveSize';

export default StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    top: 50,
  },
  contentContainer: {
    paddingTop:15,
    paddingBottom: 50,
  },
});
